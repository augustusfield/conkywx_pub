===========
DESCRIPTION
===========

Gets the weather data from `wunderground <http://www.wunderground.com>`_
website and displays the information using conky. This just helps in not having a web page open all the time. Having the web page open has it's own benefits since not all the information is available here.

Visit http://foreverquest.blogspot.in for program update information.

Documentation
-------------------------------------
Please see the `conkywx wiki <https://bitbucket.org/plikhari/conkywx_pub/wiki>`_ - it has all the documentation about the program and is also available as ``man conkywx`` after the program is installed.



Screenshots
-----------------------------------

`Screenshot gallery <http://www.zimagez.com/galerie/Conkywx-Screenshots-30244-0.php>`_
 .. image:: http://www.zimagez.com/miniature/conky-exp.jpg

There are many templates you can use from the examples directory or make your own templates.

